; Drush API
api = 2

; Core
core = 7.x
projects[drupal][type] = core
projects[drupal][version] = 7.50

; Modules
projects[addressfield] = 1.2
projects[ctools] = 1.9
projects[date] = 2.9
projects[entityreference] = 1.1
projects[entityreference_prepopulate] = 1.6
projects[features] = 2.10
projects[fontawesome] = 2.5
projects[import] = 1.0-alpha1
projects[jquery_update] = 2.7
projects[libraries] = 2.3
projects[migrate] = 2.8
projects[migrate_extras] = 2.5
projects[module_filter] = 2.0
projects[pm] = 3.0-alpha1
projects[pmgantt] = 2.0-beta1
projects[pm_dc] = 1.x-dev
projects[r4032login] = 1.8
projects[role_export] = 1.0
projects[views] = 3.14

; Themes
projects[bootstrap] = 3.5
projects[pm_kickstart_theme] = 3.0-alpha5

; Libraries
libraries[fontawesome][download][type] = "git"
libraries[fontawesome][download][url] = "https://github.com/FortAwesome/Font-Awesome.git"
libraries[fontawesome][download][tag] = "v4.6.2"
