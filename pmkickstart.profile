<?php
/**
 * @file
 * PM Kickstart distribution configuration.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Alter the site configuration form.
 */
function pmkickstart_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_block_view_alter().
 *
 * Alter the "Powered by Drupal" block.
 */
function pmkickstart_block_view_alter(&$data, $block) {
  if ($block->delta == 'powered-by') {
    $data['content'] = '<span>' . t('Project Management by <a href="@poweredby">Drupal PM</a>', array('@poweredby' => 'https://www.drupal.org/project/pm')) . '</span>';
  }
}
